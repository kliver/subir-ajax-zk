function subirGrid() {

	var grid = jq("$gridSubir @rows @row");
	var dict = []; // create an empty array
	grid.each(function(i, fila) {
		var columnas = jq("@label", fila);
		var fila_dict = {};
		columnas.each(function(j, columna) {
			var key = zk.Widget.$(columna).$n().getAttribute("key");
		
			fila_dict[key] = zk.Widget.$(columna).getValue();

		});
		dict.push(fila_dict);
	});

	console.log();
	
	dict = JSON.stringify(dict);
	jq.ajax({
        url: "recibir.zul",
        type: "post",
        data: dict,
        // callback handler that will be called on success
        success: function(response, textStatus, jqXHR){
            jq('$content').html(response);
        }
    });

}
