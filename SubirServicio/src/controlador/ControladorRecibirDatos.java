package controlador;

import java.io.IOException;

import javax.servlet.ServletRequest;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Html;

import com.google.common.io.CharStreams;


public class ControladorRecibirDatos extends SelectorComposer<Component>{

	@Wire
	Html htmlDatos;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		System.out.println("TEST");
	}
	
	@Listen("onCreate =#winRecibir")
	public void recibirDatos()
	{
		ServletRequest sr = (ServletRequest) Executions.getCurrent().getNativeRequest();

		try {
			String solicitud = CharStreams.toString(sr.getReader());
			htmlDatos.setContent(solicitud);
			
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
